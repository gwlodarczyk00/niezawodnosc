package pl.wat.wlodarczyk.niezawodnosc.solver;

import pl.wat.wlodarczyk.niezawodnosc.model.Result;

public class MorandaSolver {


    public static Result computeMorandaModel(int startingN, int[] times, double epsilonDest) {
        System.out.println("Started computing Moranda's model...");
        Result result = new Result();
        int morandaN = 0;
        double morandaFi = 0;
        double morandaEx = 0;

        double epsilon = Double.MAX_VALUE;
        boolean firstRound = true;
        int n = startingN;

        while (epsilon > epsilonDest || firstRound) {
            firstRound = false;
            double left = 0;
            double right = 0;

            left = calculateLeftSideOfMoranda(n, times);
            right = calculateRightSideMoranda(n, times);

            epsilon = Math.abs(left - right);

            if (epsilon <= epsilonDest) {
                morandaN = n;
                morandaFi = times.length / (getLeftDenominatorFactorResultMoranda(n, times) - getRightDenominatorFactorMoranda(n, times));
                morandaEx = 1 / (morandaFi * (morandaN - times.length));
                System.out.println("N: " + n + "\t (" + left + " ~= " + right + "\t eps = " + epsilon + ")");
                System.out.println("Fi: " + morandaFi);
                System.out.println("Ex: " + morandaEx);
                System.out.println("##########################################");
                result.setFoundN(morandaN);
                result.setFoundAtEpsilon(epsilon);
                result.setComputedFi(morandaFi);
                result.setComputedEx(morandaEx);
            }
            n++;
        }
        return result;
    }

    private static double calculateLeftSideOfMoranda(int n, int[] times) {
        double result = 0;
        for (int i = 1; i <= times.length; i++) {
            result = result + (double) 1 / (n - (i - 1));
        }
        return result;
    }

    private static double calculateRightSideMoranda(int n, int[] times) {
        double counter = 0;
        double leftDenominatorFactor = 0;
        double rightDenominatorFactor = 0;

        counter = getCounterResultMoranda(n, times);
        leftDenominatorFactor = getLeftDenominatorFactorResultMoranda(n, times);
        rightDenominatorFactor = getRightDenominatorFactorMoranda(n, times);

        return counter / (leftDenominatorFactor - rightDenominatorFactor);
    }

    private static double getCounterResultMoranda(int n, int[] times) {
        double result = 0;
        for (int i = 1; i <= times.length; i++) {
            result += times[i - 1];
        }
        return times.length * result;
    }

    private static double getLeftDenominatorFactorResultMoranda(int n, int[] times) {
        double result = 0;
        for (int i = 1; i <= times.length; i++) {
            result += times[i - 1];
        }
        return n * result;
    }

    private static double getRightDenominatorFactorMoranda(int n, int[] times) {
        double result = 0;
        for (int i = 1; i <= times.length; i++) {
            result += (i - 1) * times[i - 1];
        }
        return result;
    }

}
