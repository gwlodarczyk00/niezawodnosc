package pl.wat.wlodarczyk.niezawodnosc.solver;

import pl.wat.wlodarczyk.niezawodnosc.model.Result;

public class SchickSolver {

    public static Result computeSchick(int startingN, int[] times, double epsilonDest) {
        System.out.println("Started computing Schick's model...");
        Result result = new Result();

        int schickN = 0;
        double schickFi = 0;
        double schickEx = 0;

        double epsilon = Double.MAX_VALUE;
        boolean firstRound = true;
        int n = startingN;

        while (epsilon > epsilonDest || firstRound) {
            firstRound = false;

            double left = 0;
            double right = 0;

            left = calculateLeftSideOfSchick(n, times);
            right = calculateRightSideSchick(n, times);

            epsilon = Math.abs(left - right);

            if (epsilon <= epsilonDest) {
                schickN = n;
                schickFi = getFiFromSchick(schickN, times);
                schickEx = getExpFromSchick(schickN, schickFi, times);
                System.out.println("N: " + n + "\t (" + left + " ~= " + right + "\t eps = " + epsilon + ")");
                System.out.println("Fi: " + schickFi);
                System.out.println("Ex: " + schickEx);
                System.out.println("##########################################");
                result.setFoundN(schickN);
                result.setFoundAtEpsilon(epsilon);
                result.setComputedFi(schickFi);
                result.setComputedEx(schickEx);
            }
            n++;
        }
        return result;
    }

    private static double calculateLeftSideOfSchick(int n, int[] times) {
        double counter = 2 * times.length;
        double leftDenominatorFactor = 0;
        double rightDenominatorFactor = 0;

        leftDenominatorFactor = n * getTFromSchick(n, times);
        rightDenominatorFactor = getRightDenominatorFactorResultSchick(n, times);

        return counter / (leftDenominatorFactor - rightDenominatorFactor);
    }

    private static double calculateRightSideSchick(int n, int[] times) {
        double result = 0;
        for (int i = 1; i <= times.length; i++) {
            result += 1 / ((n - (i - 1)) * getTFromSchick(n, times));
        }
        return 2 * result;
    }

    private static double getTFromSchick(int n, int[] times) {
        double result = 0;
        for (int i = 1; i <= times.length; i++) {
            result += (times[i - 1] * times[i - 1]);
        }
        return result;
    }

    private static double getRightDenominatorFactorResultSchick(int n, int[] times) {
        double result = 0;
        for (int i = 1; i <= times.length; i++) {
            result += (i - 1) * (times[i - 1] * times[i - 1]);
        }
        return result;
    }

    private static double getFiFromSchick(int schickN, int[] times) {
        return calculateRightSideSchick(schickN, times);
    }

    private static double getExpFromSchick(int schickN, double schickFi, int[] times) {
        return Math.sqrt(Math.PI / ((2 * schickFi) * ((double)schickN - times.length)));
    }

}
