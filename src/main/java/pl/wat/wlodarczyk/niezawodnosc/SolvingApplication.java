package pl.wat.wlodarczyk.niezawodnosc;

import javafx.application.Application;
import javafx.stage.Stage;
import pl.wat.wlodarczyk.niezawodnosc.gui.Gui;
import pl.wat.wlodarczyk.niezawodnosc.solver.MorandaSolver;
import pl.wat.wlodarczyk.niezawodnosc.solver.SchickSolver;

import java.util.Arrays;

public class SolvingApplication extends Application {

  public static void main(String[] args) {
    launch(args);
  }

  public void start(Stage primaryStage) throws Exception {
    new Gui(primaryStage);
  }
}
