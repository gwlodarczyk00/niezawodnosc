package pl.wat.wlodarczyk.niezawodnosc.gui;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pl.wat.wlodarczyk.niezawodnosc.model.Result;
import pl.wat.wlodarczyk.niezawodnosc.solver.MorandaSolver;
import pl.wat.wlodarczyk.niezawodnosc.solver.SchickSolver;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class Gui {

    private Text selectedFile;
    private TextArea loadedData;
    private ToggleGroup modelChoice;
    private RadioButton morandaRadioButton;
    private RadioButton schickRadioButton;
    private Button computeButton;
    private Text loadedNLength;
    private TextField epsilonValue;
    private TextArea summary;

    private int[] times;
    private int n;

    public Gui(Stage primaryStage) {
        prepareGui(primaryStage);
    }

    private void prepareGui(Stage stage) {
        Button open = new Button("Wybierz plik . . .");
        open.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        open.setOnAction(new ChooseFileButtonListener());

        selectedFile = new Text();
        loadedData = new TextArea();
        loadedData.setEditable(false);
        loadedData.setWrapText(true);

        loadedNLength = new Text("N: <wybierz plik z danymi>");

        Label labelEpsilon = new Label("Granica epsilon:");
        epsilonValue = new TextField();
        HBox boxEpsilon = new HBox();
        boxEpsilon.getChildren().addAll(labelEpsilon, epsilonValue);
        boxEpsilon.setSpacing(10);

        modelChoice = new ToggleGroup();

        morandaRadioButton = new RadioButton();
        morandaRadioButton.setText("Model Jelińskiego-Morandy");
        morandaRadioButton.setToggleGroup(modelChoice);
        morandaRadioButton.setSelected(true);

        schickRadioButton = new RadioButton();
        schickRadioButton.setText("Model Schicka-Wolvertona");
        schickRadioButton.setToggleGroup(modelChoice);

        computeButton = new Button("Rozpocznij obliczenia");
        computeButton.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
        computeButton.setOnAction(new SolveButtonListener());

        summary = new TextArea();
        summary.setEditable(false);
        summary.setWrapText(true);

        GridPane gridPane = new GridPane();

        gridPane.setPadding(new Insets(10, 10, 10, 10));
        gridPane.setVgap(5);
        gridPane.setHgap(5);
        gridPane.setAlignment(Pos.TOP_CENTER);

        ColumnConstraints col1 = new ColumnConstraints();
        ColumnConstraints col2 = new ColumnConstraints();
        ColumnConstraints col3 = new ColumnConstraints();
        col1.setPercentWidth(25);
        col2.setPercentWidth(50);
        col3.setPercentWidth(25);
        gridPane.getColumnConstraints().addAll(col1, col2, col3);

        gridPane.add(open, 0, 0);
        gridPane.add(selectedFile, 1, 0, 2, 1);
        gridPane.add(loadedData, 0, 1, 3, 1);
        gridPane.add(loadedNLength,0,2);
        gridPane.add(boxEpsilon, 0, 3,2,1);
        gridPane.add(morandaRadioButton, 1, 4);
        gridPane.add(schickRadioButton, 1, 5);
        gridPane.add(computeButton, 1, 6);
        gridPane.add(summary, 0,7,3,1);
        Scene scene = new Scene(gridPane, 460, 560);

        stage.setScene(scene);
        stage.setTitle("I7B3S4 Włodarczyk Grzegorz - Solver Application");
        stage.setResizable(false);
        stage.show();
    }

    private class ChooseFileButtonListener implements EventHandler<ActionEvent> {
        public void handle(ActionEvent e) {
            showSingleFileChooser();
        }
    }

    private class SolveButtonListener implements EventHandler<ActionEvent> {
        public void handle(ActionEvent e) {
            solve();
        }
    }

    private void showSingleFileChooser() {

        FileChooser fileChooser = new FileChooser();
        File selectedFile = fileChooser.showOpenDialog(null);

        if (selectedFile != null) {
            this.selectedFile.setText("Załadowany plik: " + selectedFile.getName());
            int length = 0;
            try (Scanner scanner = new Scanner(selectedFile)) {
                StringBuilder builder = new StringBuilder();
                while (scanner.hasNext()) {
                    builder.append(scanner.nextInt());
                    builder.append(", ");
                    length++;
                }
                builder.delete(builder.lastIndexOf(", "), builder.length());
                loadedData.setText(builder.toString());
                loadedNLength.setText("N: " + length);
                n = length;
                String[] stringArray = loadedData.getText().split(", ");
                times = new int[stringArray.length];
                for (int i = 0; i < stringArray.length; i++) {
                    String numberAsString = stringArray[i];
                    times[i] = Integer.parseInt(numberAsString);
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    private void solve() {
        System.out.println("Solving...");
        Result result = new Result();
        if(morandaRadioButton.isSelected()) {
            result = MorandaSolver.computeMorandaModel(n+1, times, Double.parseDouble(epsilonValue.getText()));
        } else {
            result = SchickSolver.computeSchick(n+1, times, Double.parseDouble(epsilonValue.getText()));
        }
        StringBuilder builder = new StringBuilder();
        builder
                .append("Otrzymane wyniki: \n")
                .append("Znalezione N: ").append(result.getFoundN()).append("\n")
                .append("Epsilon: ").append(result.getFoundAtEpsilon()).append("\n")
                .append("Wartość FI: ").append(result.getComputedFi()).append("\n")
                .append("Wartość oczekiwana: ").append(result.getComputedEx()).append("\n");

        summary.setText(builder.toString());
    }
}
