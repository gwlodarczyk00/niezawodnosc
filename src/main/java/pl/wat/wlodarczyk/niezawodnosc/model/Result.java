package pl.wat.wlodarczyk.niezawodnosc.model;

public class Result {

    private int foundN;
    private double computedFi;
    private double computedEx;
    private double foundAtEpsilon;

    public int getFoundN() {
        return foundN;
    }

    public void setFoundN(int foundN) {
        this.foundN = foundN;
    }

    public double getComputedFi() {
        return computedFi;
    }

    public void setComputedFi(double computedFi) {
        this.computedFi = computedFi;
    }

    public double getComputedEx() {
        return computedEx;
    }

    public void setComputedEx(double computedEx) {
        this.computedEx = computedEx;
    }

    public double getFoundAtEpsilon() {
        return foundAtEpsilon;
    }

    public void setFoundAtEpsilon(double foundAtEpsilon) {
        this.foundAtEpsilon = foundAtEpsilon;
    }
}
